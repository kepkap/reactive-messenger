# Reactive Messenger

A simple chat application based on Actor Model.

## Next Steps

1. Spring Boot 
2. REST API 
3. Docker image
4. Deployment scripts (ansible)


## Domain Model

1. ChatActor
2. SessionActor

## Client API

1. Chat
2. Messenger

```java

Messenger messenger=new Messenger(system);
messenger.login(user1);
messenger.login(user2);

Chat chat = messenger.startChat(user1, user2);
chat.post(user1, String.format("message %s", randomAlphabetic(5)));
chat.post(user2, String.format("message %s", randomAlphabetic(5)));

List<MessageEvent> messageEvents = chat.getMessageEvents();


```

## Persistence

Persistence is done via event sourcing model, incorporated into akka.persistence.AbstractPersistentActor

DDL Schema for Postgresql: 
```reactive-messenger/reactive-messenger-core/src/main/resources/schema-pg.sql```

## Clustered Deployment
Akka provides cluster support via rich remoting capabilities. 

see configuration details:
```reactive-messenger/reactive-messenger-core/src/main/resources/cluster-application.conf```

It is envisioned that chat application will be [dockerized][Docker].




(C) Copyright 2018

[Gradle]: https://gradle.org
[Docker]: https://docker.com