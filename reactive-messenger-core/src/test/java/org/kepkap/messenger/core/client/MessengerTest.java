package org.kepkap.messenger.core.client;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.persistence.jdbc.query.javadsl.JdbcReadJournal;
import akka.persistence.query.EventEnvelope;
import akka.persistence.query.PersistenceQuery;
import akka.stream.javadsl.Source;
import akka.testkit.javadsl.TestKit;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.kepkap.messenger.core.domain.model.event.MessageEvent;
import org.kepkap.messenger.core.system.ActorSystemHolder;
import scala.concurrent.duration.FiniteDuration;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

public class MessengerTest {


    private static ActorSystem system;

    private String user1 = "user1";
    private String user2 = "user2";

    @BeforeClass
    public static void beforeClass() {
        system = ActorSystemHolder.getInstance();
    }

    @AfterClass
    public static void afterClass() {
        TestKit.shutdownActorSystem(system);
        system = null;
    }

    @Test
    public void test() {
        final TestKit kit = new TestKit(system);


        Session.getInstance().login(user1);
        Session.getInstance().login(user2);

        Chat chat = Messenger.getInstance().getChat(user1, user2);


        chat.post(user1, String.format("message %s", randomAlphabetic(5)), kit.getRef());
        String done1 = kit.expectMsgEquals(FiniteDuration.apply(10, TimeUnit.SECONDS), "done");


        chat.post(user2, String.format("message %s", randomAlphabetic(5)), kit.getRef());
        String done2 = kit.expectMsgEquals(FiniteDuration.apply(10, TimeUnit.SECONDS), "done");

        List<MessageEvent> messageEvents = chat.getMessageEvents();
//        MessageEvents done3 = kit.expectMsgClass(FiniteDuration.apply(10, TimeUnit.SECONDS), MessageEvents.class);

        assertThat(messageEvents).isNotEmpty();

        for (MessageEvent messageEvent : messageEvents) {
            System.out.println(String.format("message: from:%s -> %s", messageEvent.getFrom(), messageEvent.getMessage()));
        }
    }

    @Ignore
    @Test
    public void testQuery() {
        JdbcReadJournal readJournal = PersistenceQuery.get(system).getReadJournalFor(JdbcReadJournal.class, JdbcReadJournal.Identifier());

        String id = "id";
        Source<EventEnvelope, NotUsed> source = readJournal.currentEventsByPersistenceId(id, 0, 500);

    }

}