package org.kepkap.messenger;

import akka.actor.ActorSystem;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.kepkap.messenger.core.client.Messenger;

public class ReactiveMessenger2551 {

    public static void main(String[] args) {

        int port = 2551;

        final Config config =
            ConfigFactory.parseString(
                "akka.remote.netty.tcp.port=" + port + "\n" +
                    "akka.remote.artery.canonical.port=" + port)
                .withFallback(ConfigFactory.parseString("akka.cluster.roles = [backend]"))
                .withFallback(ConfigFactory.load("cluster-application"));


        ActorSystem system = ActorSystem.create("ReactiveMessenger",
            config);


//        Messenger messenger = new Messenger(system);
    }
}
