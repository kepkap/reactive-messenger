package org.kepkap.messenger.core.domain.model;

import java.util.Objects;

public class ChatId {

    private final String id;

    public ChatId(String id) {
        this.id = id;
    }


    public String getId() {
        return id;
    }

    public static ChatId from(String user1, String user2) {
        return new ChatId(String.format("%s-to-%s-chat", user1, user2));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatId chatId = (ChatId) o;
        return Objects.equals(id, chatId.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
