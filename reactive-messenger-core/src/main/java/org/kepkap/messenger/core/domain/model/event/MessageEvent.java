package org.kepkap.messenger.core.domain.model.event;

public class MessageEvent implements Event {

    private final String from;
    private final String message;

    public MessageEvent(String from, String message) {
        this.from = from;
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public String getMessage() {
        return message;
    }
}
