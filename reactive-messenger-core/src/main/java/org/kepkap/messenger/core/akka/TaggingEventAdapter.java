package org.kepkap.messenger.core.akka;

import akka.persistence.journal.Tagged;
import akka.persistence.journal.WriteEventAdapter;
import org.kepkap.messenger.core.domain.model.event.Event;

import java.util.HashSet;
import java.util.Set;

public class TaggingEventAdapter implements WriteEventAdapter {


    private Tagged withTag(Object event, String tag) {
        Set<String> tags = new HashSet<>();
        tags.add(tag);
        return new Tagged(event, tags);
    }


    @Override
    public String manifest(Object event) {
        return "";
    }

    @Override
    public Object toJournal(Object event) {
        if (event instanceof Event) {
            return withTag(event, event.getClass().getSimpleName());
        }
        return event;
    }
}
