package org.kepkap.messenger.core.domain.model.event;

public class LogoutEvent implements Event {

    private final String user;

    public LogoutEvent(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
