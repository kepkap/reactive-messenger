package org.kepkap.messenger.core.domain.model.event;

import java.util.List;

public class MessageEvents {

    private final List<MessageEvent> events;

    public MessageEvents(List<MessageEvent> events) {
        this.events = events;
    }

    public List<MessageEvent> getEvents() {
        return events;
    }
}
