package org.kepkap.messenger.core.client;

import akka.actor.ActorRef;
import org.kepkap.messenger.core.domain.model.ChatActor;
import org.kepkap.messenger.core.domain.model.ChatId;
import org.kepkap.messenger.core.system.ActorSystemHolder;

public class Messenger {

    private Messenger() {
    }

    public Chat getChat(String user1, String user2) {
        Session.getInstance().check(user1);
        Session.getInstance().check(user2);

        ActorRef chatActorRef = ActorSystemHolder.getInstance().actorOf(ChatActor.props(user1, user2));

        return new Chat(chatActorRef, ChatId.from(user1, user2).getId());
    }

    public Chat getChat(String id) {

        ActorRef chatActorRef = ActorSystemHolder.getInstance().actorOf(ChatActor.props(id));

        return new Chat(chatActorRef, id);
    }

    /**
     * a singleton holder
     */
    private static class Holder {
        private static final Messenger INSTANCE = new Messenger();
    }

    public static Messenger getInstance() {
        return Holder.INSTANCE;
    }
}
