package org.kepkap.messenger.core.domain.model.command;

public class MessageCmd implements Cmd {

    private final String from;
    private final String message;

    public MessageCmd(String from, String message) {
        this.from = from;
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public String getMessage() {
        return message;
    }
}
