package org.kepkap.messenger.core.domain.model;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.persistence.AbstractPersistentActor;
import org.kepkap.messenger.core.domain.model.command.GetMessageEventsCmd;
import org.kepkap.messenger.core.domain.model.command.MessageCmd;
import org.kepkap.messenger.core.domain.model.event.MessageEvent;
import org.kepkap.messenger.core.domain.model.event.MessageEvents;

import java.util.LinkedList;
import java.util.List;

public class ChatActor extends AbstractPersistentActor {

    private final String persistenceId;

    private final List<MessageEvent> messageEvents;

    public ChatActor(String persistenceId) {
        this.persistenceId = persistenceId;
        this.messageEvents = new LinkedList<>();
    }

    public static Props props(String user1, String user2) {
        return Props.create(ChatActor.class, ChatId.from(user1, user2).getId());
    }

    public static Props props(String id) {
        return Props.create(ChatActor.class, id);
    }

    @Override
    public Receive createReceiveRecover() {
        return receiveBuilder()
            .match(MessageEvent.class, e -> {
                if (!messageEvents.contains(e)) {
                    messageEvents.add(e);
                }
            })
            .build();
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(MessageCmd.class, m -> {
                MessageEvent event = new MessageEvent(m.getFrom(), m.getMessage());
                persist(event, e -> {
                    messageEvents.add(e);
                    getSender().tell("done", ActorRef.noSender());
                });
            })
            .match(GetMessageEventsCmd.class, c -> {
                sender().forward(new MessageEvents(messageEvents), getContext());
            })
            .build();
    }

    @Override
    public String persistenceId() {
        return persistenceId;
    }
}
