package org.kepkap.messenger.core.client;

import akka.actor.ActorRef;
import akka.pattern.PatternsCS;
import akka.util.Timeout;
import org.kepkap.messenger.core.domain.model.command.GetMessageEventsCmd;
import org.kepkap.messenger.core.domain.model.command.MessageCmd;
import org.kepkap.messenger.core.domain.model.event.MessageEvent;
import org.kepkap.messenger.core.domain.model.event.MessageEvents;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Chat {

    private static final Logger log = LoggerFactory.getLogger(Chat.class);

    private final ActorRef chatActorRef;
    private final String id;


    public Chat(ActorRef chatActorRef, String id) {
        this.chatActorRef = chatActorRef;
        this.id = id;
    }

    public void post(String from, String message) {
        chatActorRef.tell(new MessageCmd(from, message), ActorRef.noSender());
    }

    public void post(String from, String message, ActorRef sender) {
        chatActorRef.tell(new MessageCmd(from, message), sender);
    }

    public String getId() {
        return id;
    }

    public List<MessageEvent> getMessageEvents() {

        CompletionStage<Object> ask = PatternsCS.ask(
            chatActorRef,
            new GetMessageEventsCmd(),
            Timeout.apply(5, TimeUnit.SECONDS));

        try {
            Object result = ask.toCompletableFuture().get();
            return ((MessageEvents) result).getEvents();
        } catch (InterruptedException | ExecutionException e) {
            log.error("exception while resolving Future", e);
            return Collections.emptyList();
        }
    }
}
