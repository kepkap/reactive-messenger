package org.kepkap.messenger.core.domain.model.event;

public class LoginEvent implements Event {

    private final String user;

    public LoginEvent(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
