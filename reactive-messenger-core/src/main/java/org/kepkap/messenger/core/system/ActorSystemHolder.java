package org.kepkap.messenger.core.system;

import akka.actor.ActorSystem;
import com.typesafe.config.ConfigFactory;

public class ActorSystemHolder {

    /**
     * a singleton holder
     */
    private static class Holder {
        private static final ActorSystem system = ActorSystem.create("ReactiveMessenger",
            ConfigFactory.load("application"));
    }

    public static ActorSystem getInstance() {
        return Holder.system;
    }
}
