package org.kepkap.messenger.core.domain.model.command;

public class LoginCmd implements Cmd {

    private final String user;

    public LoginCmd(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
