package org.kepkap.messenger.core.client;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import org.kepkap.messenger.core.domain.model.SessionActor;
import org.kepkap.messenger.core.domain.model.command.CheckUserSessionCmd;
import org.kepkap.messenger.core.domain.model.command.LoginCmd;
import org.kepkap.messenger.core.domain.model.command.LogoutCmd;
import org.kepkap.messenger.core.system.ActorSystemHolder;

public class Session {

    private final ActorRef sessionActorRef;

    private Session(ActorSystem system) {
        this.sessionActorRef = system.actorOf(SessionActor.props());
    }

    public void login(String user) {
        sessionActorRef.tell(new LoginCmd(user), ActorRef.noSender());
    }

    public void logout(String user) {
        sessionActorRef.tell(new LogoutCmd(user), ActorRef.noSender());
    }

    public void check(String user) {
        sessionActorRef.tell(new CheckUserSessionCmd(user), ActorRef.noSender());
    }


    /**
     * a singleton holder
     */
    private static class Holder {
        private static final Session INSTANCE = new Session(ActorSystemHolder.getInstance());
    }

    public static Session getInstance() {
        return Holder.INSTANCE;
    }
}
