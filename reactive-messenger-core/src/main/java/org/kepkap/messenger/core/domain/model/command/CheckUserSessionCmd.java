package org.kepkap.messenger.core.domain.model.command;

public class CheckUserSessionCmd implements Cmd {

    private final String user;

    public CheckUserSessionCmd(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
