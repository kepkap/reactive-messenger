package org.kepkap.messenger.core.domain.model;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Status.Failure;
import org.kepkap.messenger.core.domain.model.command.CheckUserSessionCmd;
import org.kepkap.messenger.core.domain.model.command.LoginCmd;
import org.kepkap.messenger.core.domain.model.command.LogoutCmd;

import java.util.LinkedList;
import java.util.List;

public class SessionActor extends AbstractActor {


    private final List<String> users = new LinkedList<>();


    @Override
    public Receive createReceive() {

        return receiveBuilder()
            .match(LoginCmd.class, loginCmd -> users.add(loginCmd.getUser()))
            .match(LogoutCmd.class, logoutCmd -> users.remove(logoutCmd.getUser()))
            .match(CheckUserSessionCmd.class, checkUserSessionCmd -> {
                boolean contains = users.contains(checkUserSessionCmd.getUser());

                if (!contains) {
                    IllegalArgumentException e = new IllegalArgumentException("todo, exception  -> user is not logged in");
                    sender().tell(new Failure(e), ActorRef.noSender());
                    throw e;
                }
                getSender().tell("ok", ActorRef.noSender());
            })
            .build();

    }

    public static Props props() {
        return Props.create(SessionActor.class);
    }
}
