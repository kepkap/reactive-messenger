package org.kepkap.messenger.core.domain.model.command;

public class LogoutCmd implements Cmd {

    private final String user;

    public LogoutCmd(String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
