package org.kepkap.messenger;

import org.kepkap.messenger.core.client.Messenger;
import org.kepkap.messenger.core.client.Session;

public class ReactiveMessenger {

    public static void main(String[] args) {
//        ActorSystem system = ActorSystem.create("ReactiveMessenger",
//            ConfigFactory.load("cluster-application"));


        Messenger.getInstance();
        Session.getInstance();
    }
}
