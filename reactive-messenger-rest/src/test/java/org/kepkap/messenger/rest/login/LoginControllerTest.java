package org.kepkap.messenger.rest.login;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Ignore;
import org.junit.Test;
import org.kepkap.messenger.http.MessengerApplication;
import org.kepkap.messenger.rest.login.UserRequest;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class LoginControllerTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new MessengerApplication();
    }

    @Test
    public void testPing() {
        String responseMsg = target("ping").request().get(String.class);
        assertEquals("pong!", responseMsg);
    }

    @Test
    public void testLoginValidation() {
        UserRequest entity = new UserRequest(null);
        Response responseMsg = target("login").request().post(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE));
        assertThat(responseMsg.getStatus()).isEqualTo(400);

        entity = new UserRequest("");
        responseMsg = target("login").request().post(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE));
        assertThat(responseMsg.getStatus()).isEqualTo(400);
    }

    @Test
    public void testLogin() {
        UserRequest entity = new UserRequest("user1");
        Response responseMsg = target("login").request().post(Entity.entity(entity, MediaType.APPLICATION_JSON_TYPE));
        assertThat(responseMsg.getStatus()).isEqualTo(200);
    }

    @Ignore
    @Test
    public void testLogout() {
        UserRequest entity = new UserRequest("user1");
        Response responseMsg = target("login").request().delete();
        assertThat(responseMsg.getStatus()).isEqualTo(200);
    }
}