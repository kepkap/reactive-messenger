package org.kepkap.messenger.rest.chat;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.kepkap.messenger.http.MessengerApplication;
import org.kepkap.messenger.rest.login.UserRequest;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.assertj.core.api.Assertions.assertThat;

public class ChatControllerTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new MessengerApplication();
    }

    @Test
    public void testChat() {

        Response responseMsg = target("login").request().post(Entity.entity(new UserRequest("user1"), MediaType.APPLICATION_JSON_TYPE));
        assertThat(responseMsg.getStatus()).isEqualTo(200);
        responseMsg = target("login").request().post(Entity.entity(new UserRequest("user2"), MediaType.APPLICATION_JSON_TYPE));
        assertThat(responseMsg.getStatus()).isEqualTo(200);


        ChatResponse chatResponse = target("chat").request().post(
            Entity.entity(new ChatRequest("user1", "user2"),
                MediaType.APPLICATION_JSON_TYPE), ChatResponse.class);
        assertThat(chatResponse).isNotNull();
        assertThat(chatResponse.getId()).isNotEmpty();
        assertThat(chatResponse.getUser1()).isEqualTo("user1");
        assertThat(chatResponse.getUser2()).isEqualTo("user2");
    }

    @Test
    public void testMessages() {

        Response responseMsg = target("login").request().post(Entity.entity(new UserRequest("user1"), MediaType.APPLICATION_JSON_TYPE));
        assertThat(responseMsg.getStatus()).isEqualTo(200);
        responseMsg = target("login").request().post(Entity.entity(new UserRequest("user2"), MediaType.APPLICATION_JSON_TYPE));
        assertThat(responseMsg.getStatus()).isEqualTo(200);


        ChatResponse chatResponse = target("chat").request().post(
            Entity.entity(new ChatRequest("user1", "user2"),
                MediaType.APPLICATION_JSON_TYPE), ChatResponse.class);
        assertThat(chatResponse).isNotNull();
        String id = chatResponse.getId();
        assertThat(id).isNotEmpty();

        MessageRequest msg = new MessageRequest("user1", "message + " + this.getClass().getSimpleName() + randomAlphabetic(10));
        Response postMessage = target(String.format("chat/%s/messages", id)).request().post(Entity.entity(msg, MediaType.APPLICATION_JSON_TYPE));
        assertThat(postMessage.getStatus()).isEqualTo(200);

        MessagesResponse messagesResponse = target(String.format("chat/%s/messages", id)).request().get(MessagesResponse.class);
        assertThat(messagesResponse).isNotNull();
        assertThat(messagesResponse.getMessages()).isNotEmpty();
    }
}