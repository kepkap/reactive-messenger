package org.kepkap.messenger.http;

import org.glassfish.jersey.server.ResourceConfig;

public class MessengerApplication extends ResourceConfig {

    public MessengerApplication() {
        packages("org.kepkap.messenger.rest");
    }
}
