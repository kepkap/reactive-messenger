package org.kepkap.messenger.rest.chat;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChatResponse {

    private final String id;

    private final String user1;

    private final String user2;

    public ChatResponse(@JsonProperty("id") String id,
                        @JsonProperty("user1") String user1,
                        @JsonProperty("user2") String user2) {
        this.id = id;
        this.user1 = user1;
        this.user2 = user2;
    }

    public String getId() {
        return id;
    }

    public String getUser1() {
        return user1;
    }

    public String getUser2() {
        return user2;
    }
}
