package org.kepkap.messenger.rest.login;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class UserRequest {

    @NotEmpty
    private final String user;

    @JsonCreator
    public UserRequest(@JsonProperty("user") String user) {
        this.user = user;
    }

    public String getUser() {
        return user;
    }
}
