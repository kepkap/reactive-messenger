package org.kepkap.messenger.rest.chat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class MessageRequest {

    @NotEmpty
    private final String from;

    @NotEmpty
    private final String message;

    @JsonCreator
    public MessageRequest(@JsonProperty("from") String from,
                          @JsonProperty("message") String message) {
        this.from = from;
        this.message = message;
    }

    public String getFrom() {
        return from;
    }

    public String getMessage() {
        return message;
    }
}
