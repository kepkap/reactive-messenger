package org.kepkap.messenger.rest.chat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class ChatRequest {

    @NotEmpty
    private final String user1;

    @NotEmpty
    private final String user2;

    @JsonCreator
    public ChatRequest(@JsonProperty("user1") String user1,
                       @JsonProperty("user2") String user2) {
        this.user1 = user1;
        this.user2 = user2;
    }

    public String getUser1() {
        return user1;
    }

    public String getUser2() {
        return user2;
    }
}
