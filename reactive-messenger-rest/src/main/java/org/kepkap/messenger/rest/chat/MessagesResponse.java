package org.kepkap.messenger.rest.chat;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class MessagesResponse {

    private final List<MessageResponse> messages;

    @JsonCreator
    public MessagesResponse(@JsonProperty("messages") List<MessageResponse> messages) {
        this.messages = messages;
    }

    public List<MessageResponse> getMessages() {
        return messages;
    }
}
