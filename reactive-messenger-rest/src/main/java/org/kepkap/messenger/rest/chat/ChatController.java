package org.kepkap.messenger.rest.chat;

import org.kepkap.messenger.core.client.Chat;
import org.kepkap.messenger.core.client.Messenger;
import org.kepkap.messenger.core.domain.model.event.MessageEvent;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedList;
import java.util.List;

@Path("chat")
public class ChatController {


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response chat(@Valid ChatRequest chatRequest) {

        Chat chat = Messenger.getInstance().getChat(chatRequest.getUser1(), chatRequest.getUser2());
        return Response.ok().entity(new ChatResponse(chat.getId(), chatRequest.getUser1(), chatRequest.getUser2())).build();
    }

    @POST
    @Path("/{id}/messages")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response message(@PathParam("id") String id, @Valid MessageRequest messageRequest) {

        Chat chat = Messenger.getInstance().getChat(id);
        chat.post(messageRequest.getFrom(), messageRequest.getMessage());
        return Response.ok().build();
    }

    @GET
    @Path("/{id}/messages")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response messages(@PathParam("id") String id) {

        Chat chat = Messenger.getInstance().getChat(id);
        List<MessageEvent> messageEvents = chat.getMessageEvents();

        List<MessageResponse> messages = new LinkedList<>();
        for (MessageEvent event : messageEvents) {
            messages.add(new MessageResponse(event.getFrom(), event.getMessage()));
        }

        return Response.ok().entity(new MessagesResponse(messages)).build();
    }


}
